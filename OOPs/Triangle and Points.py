import math
class Triangle:
    
    def __init__(self):
        self.points = []
    
    def add_points(self,point1,point2):
        if len(self.points)<6:
            self.points.append((point1,point2))
            print('point:',point1,point2,'added')
        else:
            print("Triangle can't have more than 3 points")
    
    def perimeter(self):
        dist = lambda p1,p2: math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)
        return int(dist(self.points[0],self.points[1]) + dist(self.points[1],self.points[2]) + dist(self.points[2],self.points[0]))
    
    def is_equal(self,triangle2):
        return self.points == triangle2.points
        
t1 = Triangle()

t1.add_points(0,0)
t1.add_points(0,3)
t1.add_points(4,0)

print(t1.perimeter())

t2 = Triangle()

t2.add_points(1,1)
t2.add_points(2,1)
t2.add_points(1,5)


print(t1.is_equal(t2))

t3 = Triangle()

t3.add_points(1,1)
t3.add_points(2,1)
t3.add_points(1,5)

print(t1 == t3,t1.is_equal(t3),t3.is_equal(t1),sep='\n')