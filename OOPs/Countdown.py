class Counter:
    
    value=0
    
    def incr(self):
        self.value+=1
    
    def decr(self):
        self.value-=1
        
    def incrby(self,n):
        self.value+=n
    
    def decrby(self,n):
        self.value-=n
        
a=Counter()
a.incr()
print(a.value)
a.incr()
a.incr()
print(a.value)
a.decr()
print(a.value)
a.incrby(4)
print(a.value)
a.decrby(3)
print(a.value)