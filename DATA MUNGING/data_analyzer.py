from data_extractor import dataextractor

class data_analyzer:
    
    def get_data(self,filepath,length):
        get_data_instance=dataextractor()
        data=get_data_instance.read_file(filepath,length)
        
        return data
    
    def min_diff(self, column1, column2, resultant_column, filepath, length):
        value=''
        data=self.get_data(filepath,length)
        for i in data[0]:
            i[column1]=float((i[column1].split('*'))[0])
            i[column2]=float((i[column2].split('*'))[0])
            if abs(i[column1]-i[column2]) < data[1]:
                data[1] = abs(i[column1]-i[column2])
                value=i[resultant_column]
        return value