class dataextractor:
    
    min_value = 999999999999
    data = []
    
    
    def read_file(self,file_path, length):
        
        with open(file_path) as file:
            next(file)
            for row in file:
                line = row.split()
                if len(line) > length:
                    self.data.append(line)
        return [self.data,self.min_value]