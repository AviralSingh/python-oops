from data_analyzer import data_analyzer

class calculator:
    filepath=''
    length=0
    column1=0
    column2=0
    resultant_column=0
    
    def __init__(self,filepath,length,column1,column2,resultant_column):
        self.filepath=filepath
        self.length=length
        self.column1=column1
        self.column2=column2
        self.resultant_column=resultant_column
        
    def calculate(self):
        data_analyzer_instance = data_analyzer()
        print(data_analyzer_instance.min_diff(self.column1, self.column2, self.resultant_column, self.filepath, self.length))